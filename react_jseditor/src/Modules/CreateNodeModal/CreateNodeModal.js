import React, { Component } from 'react';

class CreateNodeModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            type: 'folder'
        };
    }

    onChangeInput = (e) => {
        this.setState({name: e.target.value});
    };

    onChangeSelect = (e) => {
        let type = 'file';
        if (e.target.value === 'dir') type = 'folder';
        this.setState({type: type});
    };

    render() {
        const style = {
            position: 'absolute',
            top: '100px',
            left: '100px',
            height: '100px',
            width: '300px',
            border: '1px solid #666',
            borderRadius: '5px',
            backgroundColor: '#ccc',
            zIndex: '10',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-around'
        };

        return (
            <div style={style}>
                <label>Name: <input value={this.state.name} type="text" onChange={this.onChangeInput}/></label>
                <label>Type: <select onChange={this.onChangeSelect}>
                        <option value="dir">Directory</option>
                        <option value="file">File</option>
                    </select>
                </label>
                <button onClick={(e) => {this.props.onBtnClick(this.state.name, this.state.type)}}>Create</button>
            </div>
        );
    }
}

export default CreateNodeModal;