import React, { Component } from 'react';
import './Tabs.css';

class Tabs extends Component {
    constructor({tabs, activeTab, onChangeTab, onCloseTab }) {
        super();
        this.changeTab = onChangeTab;
        this.onCloseTab = onCloseTab;
        this.state = {
            tabs: tabs,
            activeTab: activeTab
        }
    }

    toggleActiveTab = (e) => {
        let path = e.target.dataset['path'];
        this.changeTab(path);
    };

    closeTab = (e) => {
        e.stopPropagation();
        let path = e.target.dataset['path'];
        this.onCloseTab(path);
    };

    render() {
        return (
            <header className="editor_tabs">
                {this.props.tabs.map((tab, index) =>
                    <span key={`tab-${index}`}
                          data-path={tab.path}
                          className={`tab${tab.path === this.props.activeTab ? " active" : ""}`}
                          onClick={this.toggleActiveTab}>
                        {tab.name}
                        <span data-path={tab.path} onClick={this.closeTab} style={{marginRight: '3px', marginLeft: '5px'}}>X</span>
                    </span>
                )}
            </header>
        );
    }
}

export default Tabs;