import React, { Component } from 'react';
import Controls from './Controls/Controls';
import Tabs from './Tabs/Tabs';
import CodeMirror from 'react-codemirror';
import '../../../node_modules/codemirror/lib/codemirror.css';
import '../../../node_modules/codemirror/mode/javascript/javascript';
import './Editor.css';
import { connect } from 'react-redux';
import { changeFile, changeActive, closeTab } from '../../actions/actionFile';

let timer = null;

class Editor extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        let {files, activeFile} = this.props;
        if (!activeFile) activeFile = {};
        this.setState({
            value: activeFile.content,
            activeFilePath: '',
            activeFile: files[activeFile],
            isReload: false,
        });
        let isResizing = false;
        const resizer = document.querySelector('.resizer');
        let lastPosX = 0;
        const app = document.querySelector('.App');
        const editor = document.querySelector('.editor-wrapper');
        resizer.addEventListener('mousedown', e => {
            isResizing = true;
            lastPosX = e.clientX;
        });
        app.addEventListener('mousemove', e => {
            if (isResizing) {
                let d = lastPosX - e.clientX;
                lastPosX = e.clientX;
                editor.style.width = editor.clientWidth - d + 'px';
                app.style.gridTemplateColumns = `10% ${editor.style.width} auto`;
            }
        });
        app.addEventListener('mouseup', e => {
            isResizing = false;
        });
    }

    onChangeHandler = e => {
        const { activeFile } = this.state;
        this.setState(state => ({
            ...state,
            value: e,
            activeFile: {
                ...activeFile,
                content: e
            }
        }));

        clearTimeout(timer);
        timer = setTimeout(() => {
            localStorage.setItem(this.state.activeFile.path, this.state.activeFile.content);
            const { changeFile } = this.props;
            changeFile(activeFile.path, e);
        }, 500);
    };

    onClickRunHandler = () => {
        let run;
        try {
            run = new Function(this.state.value);
            run();
        } catch(e) {
            console.error(`Something went wrong. We can't compile your code, check it, please.`);
        }
        console.writeAll();
    };

    changeTab = path => {
        const { changeActive } = this.props;
        changeActive(path);
        this.setState(state => ({
            ...state,
            isReload: true
        }));

        setTimeout(() => {
            const activeFile = this.props.activeFile;
            this.setState(state => ({
                ...state,
                value: activeFile.content,
                activeFile: {
                    content: activeFile.content,
                    name: activeFile.name,
                    path: activeFile.path
                },
                activeFilePath: activeFile.path,
                isReload: false
            }));
        }, 1000);
    };

    closeTab = path => {
        const { closeTab } = this.props;
        closeTab(path);
        this.setState(state => ({
            ...state,
            isReload: true
        }));

        setTimeout(() => {
            const { activeFile } = this.props;
            this.setState(state => ({
                ...state,
                value: '',
                activeFile: { ...activeFile },
                activeFilePath: '',
                isReload: false
            }));
        }, 1000);
    };

    render() {
        let options = {
            lineNumbers: true,
            mode: 'javascript'
        };

        const { tabs } = this.props;

        return (
            <section className='editor-wrapper'>
                <Tabs tabs={tabs}
                      activeTab={this.state.activeFilePath}
                      onChangeTab={this.changeTab}
                      onCloseTab={this.closeTab}
                />
                {!this.state.isReload
                && <CodeMirror value={this.state.value} options={options} onChange={this.onChangeHandler} />
                || <div className="editor-loader_outer"><div className="editor-loader_inner"></div></div> }
                <Controls onClick={this.onClickRunHandler} />
                <div className="resizer"></div>
            </section>
        )
    }
}

export default connect(state => ({
    files: state.files.files,
    activeFile: state.files.activeFile,
    tabs: state.files.tabs
}), { changeFile, changeActive, closeTab })(Editor);