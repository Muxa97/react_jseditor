import React from 'react';

function Controls ({ onClick }) {
    return (
        <footer className='editor_controls'>
            <button onClick={onClick}>Run</button>
        </footer>
    )
}

export default Controls;
