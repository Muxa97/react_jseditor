import React, { Component } from 'react';
import './ResultView.css';
import {connect} from "react-redux";

function ResultView(props) {
        const { content } = props.activeFile || '';

        let cont = `
            <script>
                ${content}
            </script>
        `;

        return (
            <section className='resultView-wrapper'>
                <iframe srcDoc={cont} />
            </section>
        )
}

export default connect(state => ({
    activeFile: state.files.activeFile
}))(ResultView);