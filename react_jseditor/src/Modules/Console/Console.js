import React, { Component } from 'react';
import Row from './Row/Row';
import './Console.css';

const prevLog = console.log;
const prevError = console.error;
const prevClear = console.clear;
const rowBuffer = [];

class Console extends Component {
    constructor(props) {
        super(props);
        this.state = {
            code: props.code,
            rows: []
        };
    }

    componentDidMount() {
        console.log = (...args) => {
            prevLog(...args);
            rowBuffer.push({
                type: 'log',
                arguments: args
            });
        };

        console.clear = () => {
            prevClear();
            rowBuffer.length = 0;
        };

        console.error = (...args) => {
            prevError(...args);
            rowBuffer.push({
                type: 'error',
                arguments: args
            });
        };

        console.writeAll = () => {
            this.setState({
                ...this.state,
                rows: [ ...rowBuffer ]
            });
            rowBuffer.length = 0;
        };
    }

    render() {
        const rows = this.state.rows;
        const Rows = rows.map((row, index) => {
            return (
               <Row key={index} index={index} children={row} />
            );
        });

        return (
            <section className="console-wrapper">
                <header className="console-header">Console</header>
                <div className="console">
                    {Rows}
                </div>
            </section>
        );
    }
}

export default Console;