import React, { Fragment, Component } from 'react';

class ObjectRow extends Component {
    constructor(props) {
        super(props);
        const { value } = props;
        this.state = {
            obj: {...value}
        }
    }


    render() {
        let key_value = '{';
        for (const key in this.state.obj) {
            key_value += `${key}:${this.state.obj[key]}, `;
        }
        key_value.slice(-2);
        key_value += '}';

        return (
            <span className="console-row__child">{key_value}</span>
        )
    }
}

function FunctionRow({ value }) {
    return (
        <Fragment>
            <div className="console-row__child">{value.toString()}</div>
        </Fragment>
    )
}

function Row(props) {
    const children = props.children.arguments;
    const isError = props.children.type === 'error';
    const elements = children.map((element, index) => {
        if (typeof element === 'object') {
            return (<ObjectRow key={index} value={element} />);
        } else if (typeof element === 'function') {
            return (<FunctionRow key={index} value={element} />)
        } else {
            return (<span key={index} className="console-row__child">{element}</span>)
        }
    });

    let classes = 'console-row';
    if (isError) {
        classes += ' error';
    }


    return (
        <Fragment>
            <label className={classes}>{props.index + 1}{elements}</label>
        </Fragment>
    );
}

export default Row;