import React from 'react';

const getLabel = (node) => node.path.split('/').pop();

const TreeNode = (props) => {
  const { node, getChildNodes, onToggle, onNodeSelect, openFile, onRightClick, removeFile } = props;
  const s = {
    backgroundColor: node.selected ? '#22f' : '#fff',
    color: node.selected ? '#fff' : '#000',
    width: '100%',
    boxSizing: 'border-box',
    paddingLeft: '5px',
    display: 'flex',
    justifyContent: 'space-between'
  };

  return (
    <React.Fragment>
      { node.type === 'folder' && <div style={s} onDoubleClick={() => onToggle(node)}
                                       onClick={() => onNodeSelect(node)} onContextMenu={(e) => {
                                         e.preventDefault();
                                         onRightClick(node)
                                       }}>
        {node.isOpen ? '^' : ' '} {getLabel(node)} {!node.isRoot && <span onClick={() => removeFile(node)}>X</span>}
      </div> }
      { node.type === 'folder' &&
      <ul style={{width: '100%'}}>
        {node.isOpen && getChildNodes(node).map(childNode => {
          if (!!childNode)
            return <TreeNode
              key={childNode.path}
              {...props}
              node={childNode}
            />
        })}
      </ul>}
      {node.type === 'file' && <li style={s} onClick={() => onNodeSelect(node)} onDoubleClick={() => {openFile(node)}}>
        {getLabel(node)}<span  onClick={(e) => {e.stopPropagation(); removeFile(node);}}>X</span>
      </li>}
    </React.Fragment>
  );
};

export default TreeNode;