import React, { Component } from 'react';
import Tree from './Tree';
import { project } from '../../App';

class Explorer extends Component {
  constructor() {
    super();
    this.state = {
      dirs: []
    }
  };

  render() {
    return (
      <section className='explorer-wrapper'>
        <span>{project().projectName}</span>
        <Tree/>
      </section>
    )
  }
}

export default Explorer;