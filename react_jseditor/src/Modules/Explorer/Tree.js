import React, { Component } from 'react';

import TreeNode from './TreeNode';
import CreateNodeModal from '../CreateNodeModal/CreateNodeModal';
import {connect} from "react-redux";
import { changeActive, createNode, openFile, removeNode } from "../../actions/actionFile";

class Tree extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nodes: props.files,
      showModal: false,
      newFilePath: ''
    };
  }

  getRootNodes = () => {
    const { nodes } = this.props;
    return Object.values(nodes).filter(node => node.isRoot);
  };

  getChildNodes = (node) => {
    const { nodes } = this.props;
    if (!node.children) return [];
    return node.children.map(path => nodes[path]);
  };

  onToggle = (node) => {
    const { nodes } = this.props;
    nodes[node.path].isOpen = !node.isOpen;
    this.setState({ nodes });
  };

  onNodeSelect = (node) => {
    let { nodes } = this.props;
    for (const key in nodes) {
      nodes[key].selected = false;
    }

    if (nodes[node.path]) {
      nodes[node.path].selected = !nodes[node.path].selected;
    }

    this.setState({ nodes });
  };

  onRightClick = (node) => {
    let { nodes } = this.props;
    for (const key in nodes) {
      nodes[key].selected = false;
    }
    nodes[node.path].selected = !nodes[node.path].selected;
    this.setState({ nodes: nodes, showModal: true, newFilePath: node.path });
  };

  openFile = (node) => {
    const { openFile } = this.props;
    openFile(node.path);
  };

  addFile = (name, type) => {
    if (name !== '') {
      const {createNode} = this.props;
      createNode(name, this.state.newFilePath, type);
      this.setState({nodes: {...this.props.files}, showModal: false, newFilePath: ''});
    }
  };

  removeFile = ({ path }) => {
    const { removeNode } = this.props;
    const { nodes } = this.props;
    console.log(nodes, path);
    if (nodes[path] && nodes[path].type === 'folder') {
      nodes[path].children.forEach(child => {
        this.removeFile({path: child});
      })
    }

    removeNode(path);
  };

  render() {
    const rootNodes = this.getRootNodes();
    return (
      <div>
        {this.state.showModal && <CreateNodeModal onBtnClick={this.addFile}/>}
        {rootNodes.map(node => (
          <TreeNode
            key={node.path}
            node={node}
            getChildNodes={this.getChildNodes}
            onToggle={this.onToggle}
            onNodeSelect={this.onNodeSelect}
            openFile={this.openFile}
            onRightClick={this.onRightClick}
            removeFile={this.removeFile}
          />
        ))}
      </div>
    )
  }
}

export default connect(state => ({
  nodes: state.files.files,
  activeFile: state.files.activeFile
}), { changeActive, createNode, openFile, removeNode })(Tree)