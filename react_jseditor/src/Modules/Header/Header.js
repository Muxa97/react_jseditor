import React from 'react';
import Menu from './Menu/Menu';
import './Header.css';

function Logo() {
    return (
        <img className="logo" src="https://upload.wikimedia.org/wikipedia/commons/a/a7/React-icon.svg" />
    );
}

function Auth() {
    return (
        <span className="auth menu-item">Auth</span>
    )
}

function Header() {
    return (
        <header className='app-header'>
            <Logo />
            <Menu />
            <Auth />
        </header>
    )
}

export default Header;