import React, { Component, Fragment } from 'react';
import './Menu.css';
import { Link } from 'react-router-dom';

class MenuItem extends Component {
    constructor({name, actions}) {
        super();
        this.state = {
            name,
            actions: [...actions]
        }
    }

    render() {
        const actions = this.state.actions.map(action =>
            <span key={`${this.state.name}-${action}`} className="menu_action" data-actionname={action}>{action}</span>
        );
        return (
            <Fragment>
                <span data-item={this.state.name} className="menu-item">
                    {this.state.name}
                    <div className={`menu ${this.state.name}-menu`} data-menuitem={this.state.name}>
                        {actions}
                    </div>
                </span>
            </Fragment>
        )
    }
}

function Menu() {
    return (
        <Fragment>
            <span className="menu-item">
                <Link to="/">Home</Link>
            </span>
            <MenuItem name="File" actions={['Add', 'Save', 'Delete']} />
            <MenuItem name="Edit" actions={['Find', 'Replace']} />
            <MenuItem name="View" actions={[]} />
        </Fragment>
    )
}

export default Menu;