import React, { Component } from 'react';
import Console from './Modules/Console/Console';
import Editor from './Modules/Editor/Editor';
import Explorer from './Modules/Explorer/Explorer';
import Footer from './Modules/Footer/Footer';
import Header from './Modules/Header/Header';
import ResultView from './Modules/ResultView/ResultView';
import './App.css';
import {connect} from "react-redux";
import { loadStore } from './actions/actionFile';

let projectName = 'empty project';
class App extends Component {
  constructor({ match }) {
      super();
      let project = match.params.projectName;
      if (!project) project = 'empty project';

      projectName = project;
      this.state = {
          project: project
      };
  }

  componentDidMount() {
      const { loadStore } = this.props;
      loadStore(this.state.project);
  }

  componentWillUpdate(nextProps, nextState, nextContext) {
    projectName = nextProps.match.params.projectName;
    if (!projectName) projectName = 'empty project';
  }

    render() {
    return (
      <div className="App">
        <Header />
        <Explorer />
        <Editor />
        <ResultView />
        <Console />
      </div>
    );
  }
}

export default connect(store => ({ state: {...store} }), { loadStore })(App);
export const project = () => ({projectName: projectName});
