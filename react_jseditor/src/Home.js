import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Home.css';

class Home extends Component {
    constructor() {
        super();
        this.state = {
            projects: ['store', 'new_project'],
            newProjectName: ''
        }
    }

    componentDidMount() {
        const projects = JSON.parse(localStorage.getItem('projects'));

        if (!!projects) {
            this.setState({
                projects: projects
            })
        }
    }

    onChangeHandler = (e) => {
        this.setState({
            newProjectName: e.target.value
        });
    };

    newProject = () => {
        const newProjects = [ ...this.state.projects, this.state.newProjectName ];
        localStorage.setItem('projects', JSON.stringify(newProjects));
        this.setState({
            projects: newProjects,
            newProjectName: ''
        });
    };

    removeProject = (e) => {
        const projectToRemove = e.target.dataset['project'];
        const projects = this.state.projects.filter(project => project !== projectToRemove);
        localStorage.setItem('projects', JSON.stringify(projects));
        this.setState(state => ({
            ...state,
            projects: [ ...projects ]
        }));
    };

    render() {
        return (
            <section className="home-page">
                <label>Your projects
                    <ul>
                    {this.state.projects.map(projectName => (
                        <li key={projectName}>
                            <Link className="project-link" to={`/${projectName}`}>{projectName}</Link>
                            <span data-project={projectName} onClick={(e) => {
                                this.removeProject(e);
                            }}>X</span>
                        </li>
                    ))}
                    </ul>
                </label>
                <label className="add-project">New project:
                    <input className="add-project_input" value={this.state.newProjectName} type="text" onChange={this.onChangeHandler} />
                    <button className="add-project_button" onClick={this.newProject}>Create</button>
                </label>
            </section>
        )
    }
}

export default Home;