import ACTIONS from './actions';

export const createNode = (name, path, type) => ({
    type: ACTIONS.CREATE_NODE,
    name: name,
    path: path,
    nodeType: type
});

export const removeNode = (path) => ({
    type: ACTIONS.REMOVE_NODE,
    path: path,
});

export const openFile = (path) => ({
    type: ACTIONS.OPEN_FILE,
    path: path
});

export const closeTab = (path) => ({
    type: ACTIONS.CLOSE_TAB,
    path: path
});

export const changeFile = (path, content) => ({
    type: ACTIONS.CHANGE_FILE,
    path, content
});

export const changeActive = (path) => ({
    type: ACTIONS.CHANGE_ACTIVE,
    path
});

export const loadStore = (project) => ({
    type: ACTIONS.LOAD_STORE,
    project: project
});