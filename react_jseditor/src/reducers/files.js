import ACTIONS from '../actions/actions';
import { project } from '../App';

const data = {
    files: {
        '/root': {
            path: '/root',
            type: 'folder',
            isRoot: true,
            children: [],
        },
    },
    activeFile: {},
    tabs: []
};
let initialState = JSON.parse(localStorage.getItem(project().projectName));
if (!initialState) initialState = {...data};

const files = (state = initialState, action) => {
    let newState = {};
    let { files, tabs, activeFile } = state;
    switch(action.type) {
        case ACTIONS.CREATE_NODE:
            let node = {};
            if (action.nodeType === 'file') {
                node.path = action.path + '/' + action.name;
                node.type = action.nodeType;
                node.content = '';
            } else {
                node.path = action.path + '/' + action.name;
                node.type = action.nodeType;
                node.children = [];
            }

            files[action.path].children.push(node.path);
            files[node.path] = node;

            if (node.type === 'file') tabs.push({ name: action.name, path: node.path });

            newState = {
                ...state,
                files: { ...files },
                tabs: [ ...tabs ]
            };
            break;
        case ACTIONS.REMOVE_NODE:
            if (action.path in files) {
                delete files[action.path];
                for (const key in files) {
                    if (files[key].type === 'folder')
                        files[key].children = files[key].children.filter(child => child !== action.path);
                }

                tabs = tabs.filter(tab => tab.path !== action.path);

                if (activeFile.path === action.path) {
                    activeFile = null;
                }
            }
            newState = {
                activeFile: { ...activeFile },
                files: { ...files },
                tabs: [ ...tabs ]
            };
            break;
        case ACTIONS.OPEN_FILE:
            const name = action.path.split('/').pop();
            tabs = [ ...state.tabs ];
            if (!tabs.some(tab => tab.path === action.path)) {
                tabs.push({ name: name, path: action.path });
            }
            newState = {
                ...state,
                activeFile: { ...state.activeFile, path: action.path, content: localStorage.getItem(action.path) },
                tabs: [
                    ...tabs
                ]
            };
            break;
        case ACTIONS.CLOSE_TAB:
            tabs = [ ...state.tabs ];
            tabs = tabs.filter(tab => tab.path !== action.path);
            newState = {
                ...state,
                tabs: [ ...tabs ]
            };
            break;
        case ACTIONS.CHANGE_ACTIVE:
            return {
                ...state,
                activeFile: { path: action.path, content: localStorage.getItem(action.path) }
            };
        case ACTIONS.CHANGE_FILE:
            newState = {
                ...state,
                activeFile: { ...state.activeFile, content: localStorage.getItem(action.path) }
            };
            break;
        case ACTIONS.LOAD_STORE:
            //console.log(project().projectName);
            newState = {
                ...JSON.parse(localStorage.getItem(project().projectName))
            };
            break;
        default:
            newState = state;
    }

    if (!newState.hasOwnProperty('files')) {
        newState = { ...data };
    }

    localStorage.setItem(project().projectName, JSON.stringify(newState));
    return newState;
};

export default files;